import matplotlib.pyplot as plt

def fibonacci(n):
    if n<=2:
        return 1
    else:
        return fibonacci(n-1)+fibonacci(n-2)

l = []
n = 20

for i in range (1,n):
    l.append(fibonacci(i))

plt.xlabel('N')
plt.ylabel('F')

plt.plot(l)
plt.show()